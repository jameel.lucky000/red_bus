import './App.css';
import Head from './components/header/header';
import {Headdata,Bannerdata,cashback_data,convenience_data,delivery_data,guarantee_data,track_data} from './components/data';
import Banner from './components/homebanner/banner';
import Coupons from './components/coupons/coupons';
import Cashback from './components/cashback/cashback';
import Guarantee from './components/guarantee/guarantee';
import Video from './components/videocontainer/video';
import Track from './components/track/track';
import Convenience from './components/convenience/convenience';
import Delivery from './components/delivery/delivery';

function App() {
  return (
   <div>
     <Head Headdata={Headdata} />
     <Banner Bannerdata={Bannerdata}/>
     <Coupons/>
     <Cashback cashback_data={cashback_data}/>
     <Guarantee guarantee_data={guarantee_data}/>
     <Video/>
     <Track track_data={track_data}/>
     <Convenience convenience_data={convenience_data}/>
     <Delivery delivery_data={delivery_data}/>
   </div>
  );
}

export default App;
