import {Component} from 'react';
import './cashback.css';

class Cashback extends Component{
  render(){
	  const{ cashback_data}=this.props
   return(
<div class="cashback-container d-flex justify-content-center bg-light mb-5 pb-5 pt-5">
	 {cashback_data.cashback.map((item)=>{
		 return(<div className="text-center bg-white shadow m-3 p-2 pl-5 pr-5 mt-5">
		<div className="text-muted font-weight-bold small">{item.name1}</div>
			<div><img src={item.img} alt="bus"/></div>
			<div className="text-muted small">{item.name2}</div>
			<div className="font-weight-normal">{item.name3}</div>
		</div>)
	 })}
		{/* <div class="text-center bg-white shadow m-3 p-2 pl-5 pr-5 mt-5">
			<div class="text-muted font-weight-bold small">Travel on RTC and Save up to Rs 175</div>
			<div><img src="https://i.postimg.cc/kG8J6N81/bus.png" alt="bus"/></div>
			<div class="text-muted small">Pay using Amazon Pay</div>
			<div class="font-weight-normal">Use code : AABBC</div>
		</div>

		<div class="text-center bg-white shadow m-3 p-2 pl-5 pr-5 mt-5">
			<div class="text-muted font-weight-bold small">Cashback up to Rs 275 on bus tickets.</div>
			<div><img src="https://i.postimg.cc/kg7JGb5s/holiday-cashback.png" alt="bus"/></div>
			<div class="text-muted small">Limited Period Offer</div>
			<div class="font-weight-normal">Use code : CCDDEE</div>
		</div>

		<div class="text-center bg-white shadow m-3 p-2 pl-5 pr-5 mt-5">
			<div class="text-muted font-weight-bold small">Cashback up to Rs 275 on bus tickets.</div>
			<div><img src="https://i.postimg.cc/d1htvc0K/amazon.png" alt="bus"/></div>
			<div class="text-muted small">Pay using Amazon Pay</div>
			<div class="font-weight-normal">Limited Period Offer</div>
		</div> */}
	</div>

   )
  }
}

export default Cashback