import {Component} from 'react';
import './delivery.css';

class Delivery extends Component{
  render(){
	  const {delivery_data} = this.props
   return(
<div class="we-promise-container bg-light p-3 pb-5">
		<div class="text-center m-4">
			<img src="https://i.postimg.cc/QN5hqb9S/promise.png" alt="promise" width="85" />
			<h2 class="text-dark m-2">WE PROMISE TO DELIVER</h2>
		</div>

		<div class="text-center d-flex">
			{delivery_data.delivery_array.map((item)=>{
				return(<div class="border bg-white w-25">
				<div class="p-4"><img src={item.img} alt="customer_care" width="91" /></div>
				<div class="mb-5 mt-4 font-weight-light lead">{item.heading}</div>
				<div class="m-4 p-1 text-muted">{item.sub_heading}</div>
			</div>)
			})}
			{/* <div class="border bg-white w-25">
				<div class="p-4"><img src="https://i.postimg.cc/wMKHptPh/maximum-choices.png" alt="maximum_choices" width="120" /></div>
				<div class="mb-5 m-4 font-weight-light lead">MAXIMUM CHOICE</div>
				<div class="m-4 p-1 text-muted">We give you the widest number of travel options across thousands of routes.</div>
			</div>

			<div class="border bg-white w-25">
				<div class="p-4"><img src="https://i.postimg.cc/Y2mqs7V6/customer-care.png" alt="customer_care" width="91" /></div>
				<div class="mb-5 mt-4 font-weight-light lead">SUPERIOR CUSTOMER SERVICE</div>
				<div class="m-4 p-1 text-muted">We put our experience and relationships to good use and are available to solve your travel issues.</div>
			</div>

			<div class="border bg-white w-25">
				<div class="p-4"><img src="https://i.postimg.cc/JnHmv3Tr/lowest-Fare.png" alt="lowest_Fare" width="120" /></div>
				<div class="mb-5 mt-4 font-weight-light lead">LOWEST PRICES</div>
				<div class="m-4 p-1 text-muted">We always give you the lowest price with the best partner offers.</div>
			</div>

			<div class="border bg-white w-25">
				<div class="p-4"><img src="https://i.postimg.cc/k4LMgYVR/benefits.png" alt="benefits" width="120" /></div>
				<div class="mb-5 mt-4 font-weight-light lead">UNMATCHED BENEFITS</div>
				<div class="m-4 p-1 text-muted">We take care of your travel beyond ticketing by providing you with innovative and unique benefits.</div>
			</div> */}
		</div>
	</div>
   )
  }
}

export default Delivery