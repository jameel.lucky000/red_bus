 const Headdata={
  navbarImage:"https://i.postimg.cc/zXFNsGkg/redbus-white.png",
  navbarItem:[
    "Bus Tickets","Hotels","Bus Hire","Pilgrimages","Help","Manage Booking","Sign In/sign Up"
  ]
};

const Bannerdata = {
  formLabels:[
    {name:"FROM",input_id:"input-label-from",label_for:"input-label-from",state:"from"},
    {name:"TO",input_id:"input-label-to",label_for:"input-label-to",state:"to"},
    {name:"ONWARD DATE",input_id:"input-label-onward-date",label_for:"input-label-onward-date",state:"onward_date"},
    {name:"RETURN DATE",input_id:"input-label-return-date",label_for:"input-label-return-date",state:"return_date"}
  ],
  formInputs:{
    class1:"d-inline-block position-relative",
    class2:"inputIcon",
    iconClass:"far fa-building",
    class3:"inputLabel-default",
    for:"input-label-from",
    input_id:"input-label-from",
    input_class:"inputForm",
    input_type:"text",
    input_list:"input-from-list",
    datalist_id:"input-from-list"
  }
};

const cashback_data={
  cashback : [
    {name1:"Travel on RTC and save up to Rs 175",
    name2:"Pay using Amazon Pay",
    name3:"Use code : AABBC",
    img : "https://i.postimg.cc/kG8J6N81/bus.png"},
    {name1:"Cashback up to Rs 275 on bus tickets.",
     name2:"Limited Period Offer",
     name3:"Use code : CCDDEE",
     img:"https://i.postimg.cc/kg7JGb5s/holiday-cashback.png"},
    {name1:"Cashback up to Rs 275 on bus tickets.",
     name2:"Pay using Amazon Pay",
     name3:"Limited Period Offer",
     img:"https://i.postimg.cc/d1htvc0K/amazon.png"}
  ]
}

const convenience_data = {
  convenience_array : [
    "Exclusive features on mobile include",
    "redBus Now - when you need a bus in the next couple of hours. Board a bus on its way.",
    "Boarding Point Navigation - Never lose your way while traviling to your boarding point!",
    "1-click Booking - Save your favourite payment options securely on redBus, and more.",
    "Download the app.",
    "Enter your mobile number below to download the redBus mobile app."
  ]
};

const delivery_data = {
  delivery_array : [
    {img:"https://i.postimg.cc/wMKHptPh/maximum-choices.png",
     heading:"MAXIMUM CHOICE",
     sub_heading:">We give you the widest number of travel options across thousands of routes."},
     {img:"https://i.postimg.cc/Y2mqs7V6/customer-care.png",
      heading:"SUPERIOR CUSTOMER SERVICE",
    sub_heading:"We put our experience and relationships to good use and are available to solve your travel issues."},
    {img:"https://i.postimg.cc/JnHmv3Tr/lowest-Fare.png",
      heading:"LOWEST PRICES",
      sub_heading:"We always give you the lowest price with the best partner offers."},
    {img:"https://i.postimg.cc/k4LMgYVR/benefits.png",
     heading:"UNMATCHED BENEFITS",
     sub_heading:"We take care of your travel beyond ticketing by providing you with innovative and unique benefits."}
  ]
}

const guarantee_data = {
  guarantee_array:[
    {name1:"Bus on time",
     name2:"Get 25% refund",
     name3:"If bus departure is delayed by 30 mins from boarding point."
    },
    {
      name1:"No bus cancellation.",
      name2:"Get 150% refund",
      name3:"Bus is cancelled without an alternate arrangment."
    },
    {name1:"Alternate assurance",
     name2:"Get 300% refund",
     name3:"Bus breaks down with no alternate arrangment within 6 hours."}
  ]
};

const track_data = {
  track_array:[
    {name1:"BUSES",
     name2:"10,000",
     name3:"Total buses currently being tracked"},
    {name1:"ROUTES",
     name2:"60,000",
     name3:"Total users using Track My Bus feature"},
    {name1:"UESRS",
     name2:"40,000",
     name3:"Total user using Track My Bus feature"}
  ]
}
export {Headdata,Bannerdata,cashback_data,convenience_data,delivery_data,guarantee_data,track_data}