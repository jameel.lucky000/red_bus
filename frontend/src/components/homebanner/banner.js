import {Component} from 'react';
import './banner.css';



class Banner extends Component{

  constructor(props){
	  super(props)
	  this.state = {
		  from:"",
		  to:"",
		  onward_date:"",
		  return_date:""
	  }
  }
  render(){

   const {Bannerdata} = this.props
   const click_handler = (event)=>{
		
		alert("Input clicked")
   };
   function submitHandler(event){
	   event.preventDefault()

   }
   const onChangeHandler = (event)=>{
		if(event.target.value){
			this.setState(prevState=>{
				
			})
		}
   }
   return (
     <div class="position-relative">
       <div>
         <div class="home-banner">
           <h1>hi</h1>
         </div>
       </div>
       <div class="input-search-container">
         <form class="d-flex justify-content-center" onSubmit={submitHandler}>
           {Bannerdata.formLabels.map((item) => {
             return (
               <div className={Bannerdata.formInputs.class1}>
                 <span className={Bannerdata.formInputs.class2}>
                   <i className={Bannerdata.formInputs.iconClass}></i>
                 </span>
                 <label
                   className={Bannerdata.formInputs.class3}
                   for={item.label_for}
                 >
                   {item.name}
                 </label>
                 <input
                   id={item.input_id}
                   className={Bannerdata.formInputs.input_class}
                   type="text"
                   list={Bannerdata.formInputs.input_list}
                   value={item.state}
                   onChange={inputHandler}
                 />
                 <datalist id={Bannerdata.formInputs.datalist_id}></datalist>
               </div>
             );
           })}
           <div class="d-inline-block position-relative">
             <span class="inputIcon">
               <i class="far fa-building"></i>
             </span>
             <label class="inputLabel-default" for="input-label-from">
               {Bannerdata.formLabels.label1}
             </label>
             <input
               id="input-label-from"
               class="inputForm"
               type="text"
               list="input-from-list"
               value={this.state.from}
               onChange={onChangeHandler}
             />
             <datalist id="input-from-list">
               <option value="Gurgaon"></option>
             </datalist>
           </div>

           <div class="d-inline-block position-relative">
             <span class="inputIcon">
               <i class="far fa-building"></i>
             </span>
             <label class="inputLabel-default" for="input-label-to">
               TO
             </label>
             <input
               id="input-label-to"
               class="inputForm"
               type="text"
               list="input-to-list"
               value={this.state.to}
               onChange={onChangeHandler}
             />
             <datalist id="input-to-list">
               <option value="Delhi"></option>
             </datalist>
           </div>

           <div class="d-inline-block position-relative">
             <span class="inputIcon">
               <i class="fas fa-calendar-alt"></i>
             </span>
             <label class="inputLabel-default" for="input-label-onward-date">
               ONWARD DATE
             </label>
             <input
               id="input-label-onward-date"
               class="inputForm"
               type="text"
               value={this.state.onward_date}
               onChange={onChangeHandler}
             />
           </div>

           <div class="d-inline-block position-relative">
             <span class="inputIcon">
               <i class="fas fa-calendar-alt"></i>
             </span>
             <label class="inputLabel-default" for="input-label-return-date">
               RETURN DATE
             </label>
             <input
               id="input-label-return-date"
               class="inputForm"
               type="text"
               value={this.state.return_date}
               onChange={onChangeHandler}
             />
           </div>

           <div class="d-inline-block position-relative">
             <input
               class="btn btn-danger rounded-0 pl-3 pr-3 pb-2"
               type="submit"
               value="Search Buses"
             />
           </div>
         </form>
       </div>
     </div>
   );
  }
}

export default Banner