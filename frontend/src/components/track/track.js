import {Component} from 'react';
import './track.css';

class Track extends Component{
  render(){
	  const {track_data} = this.props
   return(
<div class="track-my-bus bg-light p-4 mb-5 pb-5">
		<div class="mb-5">
			<div class="float-left mr-5 ml-5">
				<img src="https://i.postimg.cc/mkGxnvzj/tmb-img.png" alt="tmb_img"/>
			</div>

			<div class="d-table-cell align-middle pl-5 pr-5">
				<h3>TRACK MY BUS - Smarter Way To Travel</h3>
				<p class="text-muted">Track your bus with our ‘Track My Bus’ feature and know the exact location in real-time.Stay informed and keep your family informed!</p>
				<button class="btn btn-light btn-outline-danger">Know More</button>
			</div>
		</div>

		<hr class="w-75 border"/>

		<div class="d-flex justify-content-around p-5">
			{track_data.track_array.map((item)=>{
				return(
				<div class="text-center">
				<div class="lead text-muted font-weight-normal">{item.name1}</div>
				<h1 class="text-danger">{item.name2}</h1>
				<div class="text-muted">{item.name3}</div>
			</div>
				)
			})}
			{/* <div class="text-center">
				<div class="lead text-muted font-weight-normal">BUSES</div>
				<h1 class="text-danger">10,000</h1>
				<div class="text-muted">Total buses currently being tracked</div>
			</div>

			<div class="text-center">
				<div class="lead text-muted font-weight-normal">ROUTES</div>
				<h1 class="text-danger">60,000</h1>
				<div class="text-muted">Total routes covered by live tracking</div>
			</div>

			<div class="text-center">
				<div class="lead text-muted font-weight-normal">USERS</div>
				<h1 class="text-danger">40,000</h1>
				<div class="text-muted">Total users using Track My Bus feature</div>
		</div> */}
		</div> 
	</div>

   )
  }
}

export default Track