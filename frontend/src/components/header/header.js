import {Component} from 'react';
import './header.css';

class Head extends Component{
render(){
const {Headdata}=this.props
return(
<nav class="navbar navbar-expand-sm bg-danger navbar-dark fixed-top">
		<div class="container">
			<a class="navbar-brand" href="/#"><img src={Headdata.navbarImage} alt="redBus" width="60px"/></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="/#menu-nav">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="menu-nav">
				<div>
					<ul class="navbar-nav">{
						Headdata.navbarItem.map((item)=>{
							return(<li class="nav-item">
							<a class="nav-link text-white text-uppercase" href="/#">{item}</a>
						</li>)
						})
					}
						{/* <li class="nav-item">
							<a class="nav-link text-white text-uppercase" href="/#">{Headdata.navbarItem.item1}</a>
						</li>
						<li class="nav-item">
							<a class="nav-link text-white text-uppercase" href="/#">{Headdata.navbarItem.item2}</a>
						</li>
						<li class="nav-item">
							<a class="nav-link text-white text-uppercase" href="/#">{Headdata.navbarItem.item3}</a>
						</li>
						<li class="nav-item">
							<a class="nav-link text-white text-uppercase" href="/#">{Headdata.navbarItem.item4}</a>
						</li> */}
					</ul>
				</div>
				<div class="collapse navbar-collapse justify-content-end" id="menu-nav">
					<ul class="navbar-nav">
						<li class="nav-item">
							<a class="nav-link text-white" href="/#">{Headdata.navbarItem.item5}</a>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle text-white" href="/#" data-toggle="dropdown">
								{Headdata.navbarItem.item6}
							</a>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="/#">Link 1</a>
								<a class="dropdown-item" href="/#">Link 2</a>
								<a class="dropdown-item" href="/#">Link 3</a>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle text-white" href="/#" data-toggle="dropdown">
								<i class="fas fa-user-circle"></i>
							</a>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="/#">{Headdata.navbarItem.item7}</a>
							</div>
						</li>
					</ul>
				</div>

			</div>
		</div>
	</nav>
)}
}

export default Head;
