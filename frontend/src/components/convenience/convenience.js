import {Component} from 'react';
import './convenience.css';

class Convenience extends Component{
  render(){
	const {convenience_data} = this.props
   return(
<div class="ios-android-container position-relative mb-5">
		<div>	
			<div class="d-flex justify-content-around">
				<div class="pt-4">
					<h3>CONVENIENCE ON-THE-GO.</h3>
					<div class="w-75">
						{/* <p class="text-dark small">Exclusive features on mobile include</p>
						<p class="text-dark small">redBus NOW - when you need a bus in the next couple of hours. Board a bus on its way.</p>
						<p class="text-dark small">Boarding Point Navigation - Never lose your way while travelling to your boarding point!</p>
						<p class="text-dark small">1-click Booking - Save your favourite payment options securely on redBus, and more.</p>
						<p class="text-dark small">Download the app.</p>
						<p class="text-dark small">Enter your mobile number below to download the redBus mobile app.</p> */}
						{convenience_data.convenience_array.map((item)=>{
							return(
								<div>
									<p class="text-dark small">{item}</p>
								</div>
							)
						})}
						<form class="form-inline">
							<select class="form-control form-control-sm mr-2 mb-2">
								<option>+91</option>
							</select>
							<input class="form-control form-control-sm mr-2 mb-2 w-50" type="text" placeholder="Enter your mobile number" />
							<button class="btn btn-outline-danger btn-sm mb-2" type="submit">SMS ME THE LINK</button>
						</form>
            <button class="btn btn-dark rounded-circle mr-2"><i class="fab fa-apple"></i></button>
						<button class="btn btn-dark rounded-circle"><i class="fab fa-google-play"></i></button>
					</div>
				</div>

				<div>
					<img src="https://i.postimg.cc/TP4ftNJN/IOS-Android-device.png" alt="IOS_Android_device" width="420" />
				</div>
			</div>
		</div>
	</div>
   )
  }
}

export default Convenience