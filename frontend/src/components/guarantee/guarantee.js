import {Component} from 'react';
import './guarantee.css';

class Guarantee extends Component{
  render(){
	  const {guarantee_data} = this.props
   return(
<div class="on-time-guarantee d-flex mt-5 mb-5 p-4 border">
		<div class="pr-4">
			<img src="https://i.postimg.cc/xTDTr7DW/hero-ontime.png" alt="hero_ontime" width="80px"/>
		</div>

		<div>
			<div>
				<h2>Introducing On-Time Guarantee</h2>
				<h4 class="text-muted font-weight-normal">Leave on time, everytime</h4>
			</div>

			<div class="d-flex pt-4 pb-4">
				<p class="font-weight-light mr-3"><span><img src="https://i.postimg.cc/Z5Ym4zfD/tip-icon.png" height="23" alt='nani'/></span> Look for buses with</p>
				<p class="mr-3"><img src="https://i.postimg.cc/fyxsTzn1/otgText.png" alt='nani'/></p>
				<p class="font-weight-light mr-3">tag while booking your journey</p>
			</div>

			<div class="d-flex">
				{guarantee_data.guarantee_array.map((item)=>{
					return(
						<div>
							<h5 class="text-primary">{item.name1}</h5>
							<div class="font-weight-bold">{item.name2}</div>
							<div class="text-muted">{item.name3}</div>
						</div>
					)
				})}
				{/* <div>
					<h5 class="text-primary">Bus on time</h5>
					<div class="font-weight-bold">Get 25% refund</div>
					<div class="text-muted">If bus departure is delayed by 30 mins from boarding point.</div>
				</div>

				<div>
					<h5 class="text-primary">No bus cancellation</h5>
					<div class="font-weight-bold">Get 150% refund</div>
					<div class="text-muted">Bus is cancelled without an alternate arrangement.</div>
				</div>

				<div>
					<h5 class="text-primary">Alternate assurance</h5>
					<div class="font-weight-bold">Get 300% refund</div>
					<div class="text-muted">Bus breaks down with no alternate arrangement within 6 hours.</div>
				</div> */}
			</div>
		</div>
	</div>

   )
  }
}

export default Guarantee