import {Component} from 'react';
import './coupons.css';

class Coupons extends Component{
  render(){
   return(
<div class="position-relative">
		<div class="holiday-coupon-container bg-white p-2 shadow">
			<div class="float-left mr-4 ml-4">
				<img src="https://i.postimg.cc/XYxVCYGb/holiday.png" alt="holiday_coupon"/>
			</div>

			<div class="d-table-cell align-middle pl-2">
				<div class="lead font-weight-bold">SAVE UPTO RS 375 ON BUS TICKETS</div>
				<div class="text-muted">Use code FIRST on App</div>
			</div>
		</div>
	</div>
   )
  }
}

export default Coupons