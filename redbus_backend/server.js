// importing express from the express module
import express from "express";
import bodyParser from "body-parser";
//const mongoose=require("mongoose");
import { Mongoose } from "mongoose";

import router from './routes/bus';
//Creating an instance of express
const app = express();
//Returns middleware that only parses json
app.use(bodyParser.json());
//Returns middleware that only parses urlencoded bodies
app.use(bodyParser.urlencoded({ extended: true }));




// const routerRegister=require("./routes/register")
// const routerBus=require("./routes/bus")

// app.use("/register",routerRegister)

app.use('/bus',routerBus)

app.listen(3001, () => {
  console.log(`listening at port 3001`);
});