import mongoose from "mongoose";

/*
    creating a schema for user_register_data
    in that withrespected datatypes
*/
const admin_register_SCHEMA = new mongoose.Schema({
  name: String,
  username: String,
  password: String,
  re_enter_password: String,
  security_question: String,
});

const BUS_SCHEMA = new mongoose.Schema({
  departure_time:String,
  departure_time:String,
  arrival_time:String,
  ratings:Array,
  seat_no:Object
})

export {admin_register_SCHEMA,BUS_SCHEMA}